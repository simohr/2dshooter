﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
	public GameObject mainPanel;
	public GameObject settingsPanel;
	public Slider volumeSlider;

	public void OnPVPButtonClick()
	{
		SceneManager.LoadScene("PVPScene");
	}

	public void OnPVEButtonClick()
	{
		SceneManager.LoadScene("PVEScene");
	}

	public void OnExitButtonClick()
	{
		Application.Quit();
	}

	public void OnSettingsClick()
	{
		mainPanel.SetActive(false);
		settingsPanel.SetActive(true);
		volumeSlider.value = AudioManager.Instance.volumeLevel;
	}

	public void OnBackClick()
	{
		mainPanel.SetActive(true);
		settingsPanel.SetActive(false);
		PlayerPrefs.SetFloat("VolumeLevel", volumeSlider.value);
	}

	public void OnVolumeSliderChange()
	{
		AudioManager.Instance.volumeLevel = volumeSlider.value;
	}
}
