﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
	public PVPCharacter pvpCharacter;
	public GameObject heartPrefab;
	private List<GameObject> hearts;
    // Start is called before the first frame update
    void Start()
    {
		hearts = new List<GameObject>();
		for (int i = 0; i < pvpCharacter.maxHealth; i++)
		{
			GameObject heart = Instantiate(heartPrefab, transform);
			hearts.Add(heart);
		}
    }
	
	public void AddHearts(int number)
	{
		for (int i = 0; i < number; i++)
		{
			GameObject heart = Instantiate(heartPrefab, transform);
			hearts.Add(heart);
		}
	}

	public void DestroyHeart()
	{
		Destroy(hearts[hearts.Count-1]);
		hearts.RemoveAt(hearts.Count - 1);
	}
}
