﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class LevelManager : MonoBehaviour
{
	public PVPCharacter leftCharacter;
	public PVPCharacter rightCharacter;

	public PowerUpsSpawner powerUpsSpawner;

	public GameObject endGamePanel;
	public Text winText;

	bool isGameOver = false;

	private void Update()
	{
		if(leftCharacter.isDead)
		{
			rightCharacter.DisableHitColliders();
			rightCharacter.ShowWinnerUI();
			//leftCharacter.DisableHitColliders();
			TriggerEndGameMenu("Right player won!");
		}

		if(rightCharacter.isDead)
		{
			leftCharacter.DisableHitColliders();
			leftCharacter.ShowWinnerUI();
			//rightCharacter.DisableHitColliders();
			TriggerEndGameMenu("Left player won!");
		}
	}

	public void OnRestartClick()
	{
		SceneManager.LoadScene("PVPScene");
	}

	public void OnMainMenuClick()
	{
		SceneManager.LoadScene("MainMenu");
	}

	private void TriggerEndGameMenu(string text)
	{
		if (isGameOver)
			return;

		powerUpsSpawner.stopSpawning = true;
		powerUpsSpawner.DestroyPowerUps();
		//endGamePanel.SetActive(true);
		winText.text = text;
		StartCoroutine(TriggerEndGameMenuCoroutine());
		//Trigger menu
		isGameOver = true;
	}

	private IEnumerator TriggerEndGameMenuCoroutine()
	{
		yield return new WaitForSeconds(2);

		endGamePanel.SetActive(true);
	}
}
