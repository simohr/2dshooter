﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#pragma warning disable 649

public class EnemySpawner : MonoBehaviour
{
	[SerializeField]
	private GameObject enemyPrefab;
	[SerializeField]
	private UIManager uiManager;
	// Start is called before the first frame update
	//private PVEEnemy currentEnemy;
	private GameObject currentEnemy;

    void Start()
    {
		SpawnEnemy(0);
    }

	public void SpawnEnemy(float spawnTime)
	{
		StartCoroutine(SpawnEnemyCoroutine(spawnTime));
	}

	public void DestroyEnemy()
	{
		if(currentEnemy != null)
			Destroy(currentEnemy);
	}

	private IEnumerator SpawnEnemyCoroutine(float spawnTime)
	{
		yield return new WaitForSeconds(spawnTime);

		float randomXPosition = Random.Range(2.7f, 6.7f);
		float yPosition = -3.89f;

		Vector3 spawnPosition = new Vector3(randomXPosition, yPosition, 0);
		GameObject enemy = Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
		enemy.GetComponent<PVEEnemy>().Initialize(this, uiManager);
		currentEnemy = enemy;
	}
}
