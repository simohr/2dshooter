﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
#pragma warning disable 649

public class UIManager : MonoBehaviour
{
	public Button energyShieldButton;
	[SerializeField]
	private TextMeshProUGUI highscoreText;
	[SerializeField]
	private GameObject mainMenuPanel;

	private int highscore = 0;

	public void UpdateHighscore(int value)
	{
		highscore += value;
		highscoreText.text = highscore.ToString();
	}

	public void OnMenuButtonClick()
	{
		mainMenuPanel.SetActive(true);
	}

	public void OnResumeButtonClick()
	{
		mainMenuPanel.SetActive(false);
	}

	public void OnMainMenuButtonClick()
	{
		SceneManager.LoadScene("MainMenu");
	}
}
