﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Manager<AudioManager>
{
	public float volumeLevel = 1f;
    // Start is called before the first frame update
    void Start()
    {
		volumeLevel = PlayerPrefs.GetFloat("VolumeLevel", 1);
    }

    // Update is called once per frame
    void Update()
    {
		AudioListener.volume = volumeLevel;
    }

	private void OnDestroy()
	{
		PlayerPrefs.SetFloat("VolumeLevel", volumeLevel);
	}
}
