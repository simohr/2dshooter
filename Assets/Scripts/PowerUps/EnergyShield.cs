﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyShield : MonoBehaviour
{
	public Transform layer1 = null;
	public Transform layer2 = null;

	private SpriteRenderer sprite1;
	private SpriteRenderer sprite2;
	private SpriteRenderer sprite3;
    // Start is called before the first frame update
    void Start()
    {
		sprite1 = GetComponent<SpriteRenderer>(); 
		sprite2 = layer1.GetComponent<SpriteRenderer>();
		sprite3 = layer2.GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	void Update()
	{
		transform.Rotate(new Vector3(0, 0, 0.1f));

		layer1.Rotate(new Vector3(0, 0, 0.2f));
		layer2.Rotate(new Vector3(0, 0, 0.25f));
	}

	public void DeactivateShield()
	{
		StartCoroutine(DeactivateShieldCoroutine());
	}

	private IEnumerator DeactivateShieldCoroutine()
	{
		float timer = 0.5f;
		float step = timer / Time.deltaTime;
		while(timer > 0)
		{
			timer -= Time.deltaTime;
			transform.localScale = new Vector3(transform.localScale.x - step, transform.localScale.y - step, transform.localScale.y - step);
			yield return null;
		}
		gameObject.SetActive(false);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Axe")
		{
			Rigidbody2D axeRB = collision.GetComponent<Rigidbody2D>();
			StartCoroutine(StopProjectile(axeRB));
			//float velocityX = Mathf.Clamp((axeRB.velocity.x - 0.5f), 0, axeRB.velocity.x);
			//float velocityY = Mathf.Clamp((axeRB.velocity.y - 0.5f), 0, axeRB.velocity.y);
			//axeRB.velocity = new Vector2(velocityX, velocityY);
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag == "Axe")
		{
			Rigidbody2D axeRB = collision.GetComponent<Rigidbody2D>();
			axeRB.bodyType = RigidbodyType2D.Dynamic;
			axeRB.freezeRotation = false;
			collision.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
			//float velocityX = Mathf.Clamp((axeRB.velocity.x - 0.5f), 0, axeRB.velocity.x);
			//float velocityY = Mathf.Clamp((axeRB.velocity.y - 0.5f), 0, axeRB.velocity.y);
			//axeRB.velocity = new Vector2(velocityX, velocityY);
		}
	}

	private IEnumerator StopProjectile(Rigidbody2D axeRB)
	{
		float timer = Random.Range(0.1f, 0.15f);
			float stepX = axeRB.velocity.x * (Time.deltaTime / timer);
			float stepY = axeRB.velocity.x * (Time.deltaTime / timer);

		while(timer > 0)
		{
			timer -= Time.deltaTime;
			//axeRB.velocity = axeRB.velocity - new Vector2(stepX, stepY);
			yield return null;
		}

		axeRB.velocity = Vector2.zero;
		axeRB.bodyType = RigidbodyType2D.Kinematic;
		axeRB.freezeRotation = true;
	}
}
