﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#pragma warning disable 649
public class BombExplosion : MonoBehaviour
{
	[SerializeField]
	private Bomb bomb;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Player")
		{
			bomb.pvpCharacter = collision.GetComponentInParent<PVPCharacter>();
		}
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			bomb.pvpCharacter = collision.GetComponentInParent<PVPCharacter>();
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if(collision.tag == "Player")
		{
			bomb.pvpCharacter = null;
		}
	}
}
