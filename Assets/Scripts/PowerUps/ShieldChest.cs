﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldChest : CollectableChest
{
	public override void OnHit(PVPCharacter pvpCharacter)
	{
		pvpCharacter.EnableShield();
		Destroy(gameObject);
	}
}
