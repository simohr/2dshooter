﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FirstAidKit : CollectableChest
{
	public TextMeshProUGUI timerText;
	public Slider slider;

	private float maxHealth = 10;
	private float health;
    // Start is called before the first frame update
    void Start()
    {
		health = maxHealth;
		StartCoroutine(DestroyTimer());
    }

	public override void OnHit(PVPCharacter pvpCharacter)
	{
		health -= 1;
		slider.value = health / maxHealth;
		if(health <= 0)
		{
			pvpCharacter.Heal(3);
			Destroy(gameObject);
		}
	}

	private IEnumerator DestroyTimer()
	{
		float timer = 9;
		while (timer > 0)
		{
			timer -= Time.deltaTime;
			timerText.text = ((int)timer).ToString();
			yield return null;
		}

		Destroy(gameObject);

	}
}
