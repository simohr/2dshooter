﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BazookaChest : CollectableChest
{
	public override void OnHit(PVPCharacter pvpCharacter)
	{
		pvpCharacter.EnableBazooka();
		Destroy(gameObject);
	}
}
