﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpsSpawner : MonoBehaviour
{
	public GameObject[] powerUps;
	public bool stopSpawning = false;

	private float timer;

	private GameObject instantiatedPowerUp;
	
    // Start is called before the first frame update
    void Start()
    {
		stopSpawning = false;
		timer = Random.Range(7f, 10.5f) + Time.time;
    }

    // Update is called once per frame
    void Update()
    {
		if (stopSpawning)
			return;

		if (instantiatedPowerUp != null)
		{
			return;
		}

		if (timer < Time.time)
		{
			SpawnPowerUp();
		}
    }

	public void DestroyPowerUps()
	{
		if(instantiatedPowerUp != null)
			Destroy(instantiatedPowerUp);
	}

	private void SpawnPowerUp()
	{
		int randomIndex = Random.Range(0, powerUps.Length);
		instantiatedPowerUp = Instantiate(powerUps[randomIndex], transform.position, Quaternion.identity);
		timer = Random.Range(7.5f, 10.5f) + Time.time;
	}
}
