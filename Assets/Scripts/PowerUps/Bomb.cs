﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
#pragma warning disable 649
public class Bomb : MonoBehaviour
{
	public PVPCharacter pvpCharacter;

	[SerializeField]
	private GameObject explosionEffect;

	[SerializeField]
	private TextMeshProUGUI timerText;

	[SerializeField]
	private BombExplosion bombExplosion;

	private void Start()
	{
		StartCoroutine(BombTimer());
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			collision.gameObject.GetComponentInParent<PVPCharacter>().DeathByExplosion();
			GameObject explosion = Instantiate(explosionEffect, transform.position, Quaternion.identity);
			Destroy(explosion, 1);
			Destroy(transform.parent.gameObject);
		}
	}

	private IEnumerator BombTimer()
	{
		float timer = 9;
		while(timer > 0)
		{
			timer -= Time.deltaTime;
			timerText.text = ((int)timer).ToString();
			yield return null;
		}

		GameObject explosion = Instantiate(explosionEffect, transform.position, Quaternion.identity);
		if(pvpCharacter == null)
		{
			explosion.GetComponent<PointEffector2D>().enabled = false;
		}
		else
		{
			pvpCharacter.DeathByExplosion();
		}

		Destroy(explosion, 1);
		Destroy(transform.parent.gameObject);

	}
}
