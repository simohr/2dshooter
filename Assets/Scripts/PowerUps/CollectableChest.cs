﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CollectableChest : MonoBehaviour
{
	public abstract void OnHit(PVPCharacter pvpCharacter);
}
