﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
	public PVPCharacter pvpCharacter;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Bullet" || collision.tag == "Rocket")
		{
			if(collision.GetComponent<Bullet>().pvpCharacter != pvpCharacter)
				Destroy(collision.gameObject);
		}
	}
}
