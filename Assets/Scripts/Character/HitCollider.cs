﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#pragma warning disable 649
public class HitCollider : MonoBehaviour
{
	public int damage;

	private PVPCharacter pvpCharacter;

	private void Start()
	{
		pvpCharacter = GetComponentInParent<PVPCharacter>();
	}

	public void OnHit(bool isBullet, Vector2 hitPoint)
	{
		if (isBullet)
		{
			pvpCharacter.TakeDamage(damage, hitPoint);
		}
		else
		{
			pvpCharacter.DeathByExplosion();
		}
	}
}
