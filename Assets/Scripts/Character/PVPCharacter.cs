﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

#pragma warning disable 649
public class PVPCharacter : MonoBehaviour
{
	public int maxHealth;
	[SerializeField]
	public bool isLeft;
	[SerializeField]
	private HealthBar healthBar;

	[SerializeField]
	private GameObject winnerUI;

	[SerializeField]
	private AudioClip hurtSound;

	[SerializeField]
	private GameObject bloodSplatterEffect;

	#region GUNS
	[SerializeField]
	private GameObject gun;
	[SerializeField]
	private GameObject gunBarrel;
	[SerializeField]
	private GameObject gunBullet;
	[SerializeField]
	private AudioClip gunShot;

	[SerializeField]
	private GameObject gunFireEffect;

	[SerializeField]
	private GameObject bazooka;
	[SerializeField]
	private GameObject bazookaBarrel;
	[SerializeField]
	private GameObject bazookaRocket;
	[SerializeField]
	private AudioClip bazookaShot;

	private GameObject currentBarrel;
	private GameObject currentProjectile;
	private AudioClip currentWeaponClip;

	#endregion
	[SerializeField]
	private GameObject hitColliders;

	[SerializeField]
	private GameObject ragdollPrefabLeft;
	[SerializeField]
	private GameObject ragdollPrefabRight;

	[SerializeField]
	private GameObject shiled;

	private Rigidbody2D rb;
	private AudioSource audioSource;
	private Animator animator;

	[HideInInspector]
	public bool isDead = false;

	private int jumpCount = 0;

	private int health;

	// Start is called before the first frame update

	private void Start()
    {
		currentBarrel = gunBarrel;
		currentProjectile = gunBullet;
		currentWeaponClip = gunShot;

		if(!isLeft)
		{
			//Quaternion rotation = Quaternion.LookRotation(;
			transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
		}

		health = maxHealth;
		rb = GetComponent<Rigidbody2D>();
		audioSource = GetComponent<AudioSource>();
		animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
		if (isDead)
			return;

#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0) && !BlockClick(Input.mousePosition))
		{
			if (Input.mousePosition.x < Screen.width / 2)
			{
				if (isLeft)
					Jump();
			}
			else if (Input.mousePosition.x > Screen.width / 2)
			{
				if (!isLeft)
					Jump();
			}
		}
#endif
#if UNITY_ANDROID
		if (Input.touchCount > 0)
		{
			Touch[] touches = Input.touches;
			for (int i = 0; i < touches.Length; i++)
			{
				if (Input.GetTouch(i).phase == TouchPhase.Began)
				{
					if (BlockClick(touches[i].position))
						return;

					if (touches[i].position.x < Screen.width / 2)
					{
						if (isLeft)
							Jump();
					}
					else if (touches[i].position.x > Screen.width / 2)
					{
						if (!isLeft)
							Jump();
					}
				}
			}
		}
#endif
	}

	public void ShowWinnerUI()
	{
		if (!isLeft)
			winnerUI.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
		winnerUI.SetActive(true);
	}

	public void DeathByExplosion()
	{
		for (int i = 0; i < health; i++)
		{
			healthBar.DestroyHeart();
		}

		isDead = true;
		GetComponent<BoxCollider2D>().enabled = false;
		rb.bodyType = RigidbodyType2D.Static;

		GameObject ragdollPrefab = isLeft ? ragdollPrefabLeft : ragdollPrefabRight;
		float forceDirection = isLeft ? -1 : 1;

		GameObject ragdoll = Instantiate(ragdollPrefab, transform.position, Quaternion.identity);
		ragdoll.GetComponent<Ragdoll>().DeathByExplosion();
		Destroy(gameObject);
	}

	public void TakeDamage(int damage, Vector2 hitPoint)
	{
		if (health > 0)
		{
			UpdateHealthBar(damage);

			Quaternion bloodSplatterRotation = bloodSplatterEffect.transform.rotation;
			float positionOffset = 0.25f;
			if (!isLeft)
			{
				bloodSplatterRotation = Quaternion.Euler(new Vector3(0, 270, 0));
				positionOffset = -0.25f;
			}

			Vector3 bloodSplatterPosition = new Vector3(hitPoint.x + positionOffset, hitPoint.y, -1);

			GameObject bloodSplatter = Instantiate(bloodSplatterEffect, bloodSplatterPosition, bloodSplatterRotation, transform);
			Destroy(bloodSplatter, 2);


			if (health <= 0)
			{
				isDead = true;
				//activate ragdoll

				GameObject ragdollPrefab = isLeft ? ragdollPrefabLeft : ragdollPrefabRight;
				float forceDirection = isLeft ? -1 : 1;

				GameObject ragdoll = Instantiate(ragdollPrefab, transform.position, Quaternion.identity);
				Transform bloodSplatterParent = ragdoll.transform;

				switch (damage)
				{
					case 3:
						ragdoll.GetComponent<Ragdoll>().HeadShot(forceDirection);
						bloodSplatterParent = ragdoll.GetComponent<Ragdoll>().headRB.transform;
						break;
					case 2:
						ragdoll.GetComponent<Ragdoll>().TorsoShot(forceDirection);
						bloodSplatterParent = ragdoll.GetComponent<Ragdoll>().torsoRB.transform;
						break;
					case 1:
						ragdoll.GetComponent<Ragdoll>().LegShot(forceDirection);
						bloodSplatterParent = ragdoll.GetComponent<Ragdoll>().legRB.transform;
						break;
				}

				bloodSplatter.transform.SetParent(bloodSplatterParent, true);
				bloodSplatter.transform.localScale = new Vector3(1, 1, 1);

				Destroy(gameObject);
			}
		}
	}


	public void Shoot()
	{
		if (isDead)
			return;

		GameObject gunFire = Instantiate(gunFireEffect, currentBarrel.transform, false);
		Destroy(gunFire, 1);

		Quaternion bulletRotation = Quaternion.identity;
		if(!isLeft)
		{
			bulletRotation = Quaternion.Euler(new Vector3(0, 0, 180));
		}

		GameObject bullet = Instantiate(currentProjectile, currentBarrel.transform.position, bulletRotation);
		audioSource.PlayOneShot(currentWeaponClip);
		bullet.GetComponent<Bullet>().pvpCharacter = this;
	}

	public void Heal(int heal)
	{
		int currentHealth = health; 
		health += heal;
		health = health > maxHealth ? maxHealth : health;
		healthBar.AddHearts(health - currentHealth);
	}

	public void DisableHitColliders()
	{
		if(hitColliders.activeInHierarchy)
			hitColliders.SetActive(false);
	}

	public void EnableShield()
	{
		shiled.SetActive(true);
		if(disableShiledCoroutine != null)
		{
			StopCoroutine(disableShiledCoroutine);
		}
		disableShiledCoroutine = DisableShiled();
		StartCoroutine(disableShiledCoroutine);
	}

	public void EnableBazooka()
	{
		gun.SetActive(false);
		bazooka.SetActive(true);
		currentBarrel = bazookaBarrel;
		currentProjectile = bazookaRocket;
		currentWeaponClip = bazookaShot;

		if (disableBazookaCoroutine != null)
		{
			StopCoroutine(disableBazookaCoroutine);
		}
		disableBazookaCoroutine = DisableBazooka();
		StartCoroutine(disableBazookaCoroutine);
	}

	private IEnumerator disableBazookaCoroutine;
	private IEnumerator DisableBazooka()
	{
		float timer = 5;
		while (timer > 0)
		{
			timer -= Time.deltaTime;
			yield return null;
		}

		gun.SetActive(true);
		bazooka.SetActive(false);
		currentBarrel = gunBarrel;
		currentProjectile = gunBullet;
		currentWeaponClip = gunShot;

		disableBazookaCoroutine = null;
	}


	private IEnumerator disableShiledCoroutine;
	private IEnumerator DisableShiled()
	{
		float timer = 5;
		while(timer > 0 )
		{
			timer -= Time.deltaTime;
			yield return null;
		}

		shiled.SetActive(false);
		disableShiledCoroutine = null;
	}

	private void Jump()
	{
		if (jumpCount < 2)
		{
			animator.SetTrigger("Jump");
			rb.velocity = new Vector2(0, 7.5f);
			jumpCount++;
		}
	}

	private void UpdateHealthBar(int damage)
	{
		int heartsToDestroy = health > damage ? damage : health;
		health -= damage;
		audioSource.PlayOneShot(hurtSound);
		for (int i = 0; i < heartsToDestroy; i++)
		{
			healthBar.DestroyHeart();
		}
	}

	private bool BlockClick(Vector2 screenPosition)
	{
		PointerEventData cursor = new PointerEventData(EventSystem.current);                            
		// This section prepares a list for all objects hit with the raycast
		cursor.position = screenPosition;
		List<RaycastResult> objectsHit = new List<RaycastResult>();
		EventSystem.current.RaycastAll(cursor, objectsHit);
		foreach (var item in objectsHit)
		{
			if(item.gameObject.tag == "BlockClicks")
			{
				return true;
			}
		}

		return false;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Ground")
		{
			jumpCount = 0;
		}
	}
}
