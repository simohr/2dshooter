﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
#pragma warning disable 649

public class PVECharacter : MonoBehaviour
{
	[SerializeField]
	private LineRenderer lineRenderer;
	[SerializeField]
	private Animator animator;
	[SerializeField]
	private Transform axeTransform;
	[SerializeField]
	private GameObject axePrefab;
	[SerializeField]
	private AudioClip throwAxeClip;

	private AudioSource audioSource;
	private Vector2 fp;
	private Vector2 lp;
	private float swipeDistanceX;
	private float swipeDistanceY;
	private float angle;
	private bool clickBlocked = false;

	// Start is called before the first frame update
	void Start()
	{
		audioSource = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update()
	{
#if UNITY_EDITOR
		if(Input.GetMouseButtonDown(0))
		{
			if (BlockClick(Input.mousePosition))
			{
				clickBlocked = true;
				return;
			}
			fp = Input.mousePosition;
			lp = Input.mousePosition;
			lineRenderer.enabled = true;
			Vector3 worldPosition = Camera.main.ScreenToWorldPoint(fp);
			lineRenderer.SetPosition(0, new Vector3(worldPosition.x, worldPosition.y, 0));
		}

		if(Input.GetMouseButton(0))
		{
			if(clickBlocked)
			{
				return;
			}
			lp = Input.mousePosition;
			swipeDistanceX = Mathf.Abs((lp.x - fp.x));
			swipeDistanceY = Mathf.Abs((lp.y - fp.y));
			Vector3 worldPosition = Camera.main.ScreenToWorldPoint(lp);
			lineRenderer.SetPosition(1, new Vector3(worldPosition.x, worldPosition.y, 0));
		}

		if (Input.GetMouseButtonUp(0))
		{
			if(clickBlocked)
			{
				clickBlocked = false;
				return;
			}
			angle = Mathf.Atan2((fp.y - lp.y), (fp.x - lp.x)) * 57.2957795f;
			animator.SetTrigger("ThrowAxe");
			lineRenderer.enabled = false;
		}
#endif
#if UNITY_ANDROID
		foreach (Touch touch in Input.touches)
		{
			if (touch.phase == TouchPhase.Began)
			{
				if (BlockClick(touch.position))
				{
					clickBlocked = true;
					return;
				}
				fp = touch.position;
				lp = touch.position;
				Vector3 worldPosition = Camera.main.ScreenToWorldPoint(fp);
				lineRenderer.SetPosition(0, new Vector3(worldPosition.x, worldPosition.y, 0));
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				if (clickBlocked)
				{
					return;
				}
				lineRenderer.enabled = true;

				lp = touch.position;
				swipeDistanceX = Mathf.Abs((lp.x - fp.x));
				swipeDistanceY = Mathf.Abs((lp.y - fp.y));
				Vector3 worldPosition = Camera.main.ScreenToWorldPoint(lp);
				lineRenderer.SetPosition(1, new Vector3(worldPosition.x, worldPosition.y, 0));
			}
			if (touch.phase == TouchPhase.Ended)
			{
				if (clickBlocked)
				{
					clickBlocked = false;
					return;
				}
				angle = Mathf.Atan2((fp.y - lp.y), (fp.x - lp.x)) * 57.2957795f;
				animator.SetTrigger("ThrowAxe");
				lineRenderer.enabled = false;
			}
		}
#endif
	}

	public void ThrowAxe()
	{
		GameObject axe = Instantiate(axePrefab, axeTransform.position, axeTransform.rotation);
		audioSource.PlayOneShot(throwAxeClip);
		Rigidbody2D axeRB = axe.GetComponent<Rigidbody2D>();

		float swipeForce = Mathf.Clamp((swipeDistanceX + swipeDistanceY) / 100, 1, 3);
		axeRB.velocity = DegreeToVector2(angle) * 4 * swipeForce;
		float rotationForce = Mathf.Clamp(swipeForce / 1.3f, 1, 2);
		axeRB.AddTorque(-360 * rotationForce, ForceMode2D.Force);
		Destroy(axe, 10);
	}

	public static Vector2 RadianToVector2(float radian)
	{
		return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
	}

	public static Vector2 DegreeToVector2(float degree)
	{
		return RadianToVector2(degree * Mathf.Deg2Rad);
	}

	private bool BlockClick(Vector2 screenPosition)
	{
		PointerEventData cursor = new PointerEventData(EventSystem.current);
		// This section prepares a list for all objects hit with the raycast
		cursor.position = screenPosition;
		List<RaycastResult> objectsHit = new List<RaycastResult>();
		EventSystem.current.RaycastAll(cursor, objectsHit);
		foreach (var item in objectsHit)
		{
			if (item.gameObject.tag == "BlockClicks")
			{
				return true;
			}
		}

		return false;
	}
}
