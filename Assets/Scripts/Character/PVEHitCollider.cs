﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PVEHitCollider : MonoBehaviour
{
	public int damage;
	[SerializeField]
	private GameObject bloodSplatterEffect;
	private PVEEnemy pveEnemy;
	// Start is called before the first frame update
	void Start()
	{
		pveEnemy = GetComponentInParent<PVEEnemy>();
	}

	public void TakeAHit(Vector2 hitPoint, Transform axe)
	{
		pveEnemy.TakeDamage(damage, hitPoint, axe);
	}
}
