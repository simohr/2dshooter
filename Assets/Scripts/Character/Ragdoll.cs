﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{
	public Rigidbody2D headRB;
	public Rigidbody2D torsoRB;
	public Rigidbody2D legRB;

	public void HeadShot(float direction)
	{
		float power = Random.Range(-10, 45);
		headRB.velocity = new Vector3(direction * power,0,0);
	}

	public void TorsoShot(float direction)
	{
		float power = Random.Range(-10, 45);
		torsoRB.velocity = new Vector3(direction * power, 0, 0);
	}

	public void LegShot(float direction)
	{
		float power = Random.Range(-10, 45);
		legRB.velocity = new Vector3(direction * power, 0, 0);
	}

	public void DeathByExplosion()
	{
		foreach (Transform child in transform)
		{
			Rigidbody2D rb = child.GetComponent<Rigidbody2D>();
			if (rb != null)
			{
				rb.mass = 50;
			}
		}
	}
}
