﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public GameObject sparkEffect;
	public float speed = 5;
	public PVPCharacter pvpCharacter;
	// Update is called once per frame

	private void Start()
	{
		Destroy(gameObject, 15);
	}

	void FixedUpdate()
    {
		transform.Translate(Vector3.right * speed * Time.deltaTime);
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Bullet")
		{
			GameObject spark = Instantiate(sparkEffect, collision.transform.position, Quaternion.identity);
			Destroy(spark, 1);
			Destroy(gameObject);
		}

		if (collision.tag == "Rocket")
		{
			GameObject spark = Instantiate(sparkEffect, collision.transform.position, Quaternion.identity);
			Destroy(spark, 1);
			Destroy(gameObject);
		}

		if (collision.tag == "Bomb")
		{
			GameObject spark = Instantiate(sparkEffect, collision.transform.position, Quaternion.identity);
			Destroy(spark, 1);
			Destroy(gameObject);

			if(pvpCharacter.isLeft)
			{
				collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(35,0));
			}
			else
			{
				collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-35, 0));
			}
		}

		if(collision.tag == "Player")
		{
			if(transform.tag == "Bullet")
			{
				collision.GetComponent<HitCollider>().OnHit(true, collision.ClosestPoint(transform.position));
			}
			else if(transform.tag == "Rocket")
			{
				GameObject explosionEffect = Instantiate(sparkEffect, transform.position, Quaternion.identity);
				explosionEffect.GetComponent<CircleCollider2D>().enabled = true;
				explosionEffect.GetComponent<PointEffector2D>().enabled = true;
				Destroy(explosionEffect, 1);
				collision.GetComponent<HitCollider>().OnHit(false, collision.ClosestPoint(transform.position));
			}

			Destroy(gameObject);
		}

		if (collision.tag == "Collectable")
		{
			GameObject spark = Instantiate(sparkEffect, collision.transform.position, Quaternion.identity);
			Destroy(spark, 1);

			collision.GetComponent<CollectableChest>().OnHit(pvpCharacter);

			Destroy(gameObject);
		}
	}
}
