﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#pragma warning disable 649

public class PVEEnemy : MonoBehaviour
{
	[SerializeField]
	private GameObject[] ragdollParts;
	[SerializeField]
	private GameObject hitColliders;
	[SerializeField]
	private GameObject bloodSplatterEffect;
	[SerializeField]
	private EnergyShield shield;

	[SerializeField]
	private AudioClip hurtClip;
	[SerializeField]
	private AudioClip deadClip;

	private AudioSource audioSource;
	public Vector3 currentBloodSplatterPosition;
	private UIManager uiManager;
	private EnemySpawner enemySpawner;

	private int health = 2;
	
	private Animator animator;

	public void Initialize(EnemySpawner enemySpawner, UIManager uiManager)
	{
		animator = GetComponent<Animator>();
		this.enemySpawner = enemySpawner;
		this.uiManager = uiManager;
		uiManager.energyShieldButton.onClick.RemoveAllListeners();
		uiManager.energyShieldButton.onClick.AddListener(CastShield);
		uiManager.energyShieldButton.interactable = true;
		audioSource = GetComponent<AudioSource>();
	}

	public void TakeDamage(int damage, Vector2 hitPoint, Transform axe)
	{
		health -= damage;
		audioSource.PlayOneShot(hurtClip);
		Quaternion bloodSplatterRotation = Quaternion.Euler(new Vector3(0, 270, 0));
		float positionOffset = -0.25f;

		Vector3 bloodSplatterPosition = new Vector3(hitPoint.x + positionOffset, hitPoint.y, -1);
		Transform bloodSplatterParent = transform;
		switch (damage)
		{
			case 2:
				bloodSplatterParent = ragdollParts[0].transform;
				break;
			case 1:
				bloodSplatterParent = ragdollParts[1].transform;
				break;
		}
		axe.SetParent(bloodSplatterParent, true);
		GameObject bloodSplatter = Instantiate(bloodSplatterEffect, bloodSplatterPosition, bloodSplatterRotation);
		bloodSplatter.transform.SetParent(bloodSplatterParent, true);
		bloodSplatter.transform.localScale = new Vector3(1, 1, 1);
		Destroy(bloodSplatter, 1.5f);

		if (health <= 0)
		{
			//activate ragdoll
			audioSource.PlayOneShot(deadClip);
			//GameObject ragdoll = Instantiate(ragdollParts[0], transform.position, Quaternion.identity);
			EnableRagdoll();
			Ragdoll ragdoll = GetComponent<Ragdoll>();

			switch (damage)
			{
				case 2:
					ragdoll.HeadShot(1);
					break;
				case 1:
					ragdoll.TorsoShot(1);
					break;
			}

			enemySpawner.SpawnEnemy(2f);
			uiManager.UpdateHighscore(1);
			uiManager.energyShieldButton.interactable = true;
			Destroy(gameObject, 1.5f);
		}
	}

	public void CastShield()
	{
		animator.SetBool("Casting", true) ;
		uiManager.energyShieldButton.interactable = false;
		StartCoroutine(ShieldCooldown());
		StartCoroutine(StopCasting());
	}

	private IEnumerator ShieldCooldown()
	{
		yield return new WaitForSeconds(8);
		uiManager.energyShieldButton.interactable = true;
	}

	private IEnumerator StopCasting()
	{
		yield return new WaitForSeconds(4.5f);
		shield.DeactivateShield();
		yield return new WaitForSeconds(0.5f);
		animator.SetBool("Casting", false);
	}

	private void EnableRagdoll()
	{
		hitColliders.SetActive(false);
		GetComponent<Animator>().enabled = false;
		GetComponent<BoxCollider2D>().enabled = false;
		GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;

		foreach (GameObject part in ragdollParts)
		{
			part.GetComponent<BoxCollider2D>().enabled = true;
			part.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
			if (part.name != "Torso")
			{
				part.GetComponent<HingeJoint2D>().enabled = true;
			}
		}
	}

}
