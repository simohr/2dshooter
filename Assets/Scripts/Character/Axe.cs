﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axe : MonoBehaviour
{
	int count = 0;
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Player")
		{
			if (count != 0)
				return;
			count++;
			Vector2 hitPoint = collision.ClosestPoint(transform.position);
			collision.GetComponent<PVEHitCollider>().TakeAHit(hitPoint, transform);
			//Destroy(gameObject);
			Rigidbody2D rb = GetComponent<Rigidbody2D>();
			rb.bodyType = RigidbodyType2D.Kinematic;
			rb.velocity = Vector2.zero;
			rb.freezeRotation = true;
		}
	}
}
